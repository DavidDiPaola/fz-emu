# fz-emu
Emulator for the FZ Z80+2C02+SAA1099 DYI retro computer abomination.

## Dependencies
- SDL1.2 with SDL_Gfx (In Ubuntu/Debian: `sudo apt install libsdl1.2-dev libsdl-gfx1.2-dev`)
- Z80 GNU binutils (In Ubuntu/Debian: `sudo apt install binutils-z80`)

## Licenses
- All code is under CC0 except for what's listed below.
- The [Z80 emulator](https://github.com/superzazu/z80) was written by [superzazu](https://github.com/superzazu) and is under the MIT license. This code is under `src/lib/z80/`.
