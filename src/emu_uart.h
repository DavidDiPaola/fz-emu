#ifndef __EMU_UART_H
#define __EMU_UART_H

#include <stdint.h>

#include <stdio.h>

#define emu_uart_fifo_LENGTH 16

struct emu_uart_fifo {
	uint8_t in_pos;
	uint8_t out_pos;
	uint8_t data[emu_uart_fifo_LENGTH];
};
struct emu_uart {
	void (*char_write)(uint8_t ch);

	int clock_hz;

	/* registers (see: [1] table 2) */
	uint8_t  ier;  /* Interrupt Enable Register */
	uint8_t  isr;  /* Interrupt Status Register */
	uint8_t  fcr;  /* FIFO Control Register */
	uint8_t  lcr;  /* Line Control Register */
	uint8_t  mcr;  /* Modem Control Register */
	uint8_t  lsr;  /* Line Status Register */
	uint8_t  msr;  /* Modem Status Register */
	uint8_t  spr;  /* Scratchpad Register */
	uint16_t dl;   /* Divisor Latch */

	int clocksPerChar;
	int tx_counter;

	struct emu_uart_fifo rx_fifo;
	struct emu_uart_fifo tx_fifo;
};

/* dump the state to stdout */
void
emu_uart_dump(struct emu_uart * state);

/* simulate reset */
void
emu_uart_reset(struct emu_uart * state);

void
emu_uart_init(struct emu_uart * state, void (*char_write)(uint8_t ch), int clock_hz);

void
emu_uart_init_default(struct emu_uart * state);

/* simulate one clock cycle */
void
emu_uart_tick(struct emu_uart * state);

/* simulate reading from a UART register */
uint8_t
emu_uart_read(struct emu_uart * state, uint8_t address);

/* simulate writing to a UART register */
void
emu_uart_write(struct emu_uart * state, uint8_t address, uint8_t data);

/* simulate a character being sent into the UART's RX wire */
void
emu_uart_receive(struct emu_uart * state, uint8_t data);

#endif
