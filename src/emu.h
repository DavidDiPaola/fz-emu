#ifndef __EMU_H
#define __EMU_H

/*
initialize the emulator
	bios -- a pointer to a BIOS image. not used after this function returns.
	bios_size -- the size of the BIOS image in bytes.
*/
void
emu_init(const uint8_t * bios, size_t bios_size);

void
emu_dump_cpu(void);

/* dump memory from the perspective of the CPU */
void
emu_dump_mem(uint16_t start, uint16_t amount);

void
emu_dump(void);

void
emu_reset(void);

void
emu_step(void);

#endif
