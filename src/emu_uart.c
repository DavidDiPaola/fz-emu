/*
2022 David DiPaola. Licensed CC0.

References:
	[1] EXAR ST16C550 datasheet rev. 4.30
*/

#include <stdio.h>

#include "emu_uart.h"

/* register addresses (see: [1] table 2 and table 4) */
#define _RHR 0b000  /* Receiver Holding Register (when reading) */
#define _THR _RHR   /* Transmitter Holding Register (when writing) */
#define _IER 0b001  /* Interrupt Enable Register */
#define _ISR 0b010  /* Interrupt Status Register (when reading) */
#define _FCR _ISR   /* FIFO Control Register (when writing) */
#define _LCR 0b011  /* Line Control Register */
#define _MCR 0b100  /* Modem Control Register */
#define _LSR 0b101  /* Line Status Register (when reading) */
#define _MSR 0b110  /* Modem Status Register (when reading) */
#define _SPR 0b111  /* Scratchpad Register */
#define _DLL 0b000  /* Divisor Latch Least Significant Byte */
#define _DLM 0b001  /* Divisor Latch Most Significant Byte */

static void
_fifo_init(struct emu_uart_fifo * fifo) {
	fifo->in_pos  = 0;
	fifo->out_pos = 0;

	// TODO how to init FIFO data?
	/*
	for (uint8_t i=0; i<emu_uart_fifo_LENGTH; i++) {
		fifo->data[i] = 0;
	}
	*/
}

static int
_fifo_empty(struct emu_uart_fifo * fifo) {
	return (fifo->out_pos == fifo->in_pos);
}

static int
_fifo_full(struct emu_uart_fifo * fifo) {
	int behind_out_pos = fifo->out_pos - 1;
	if (behind_out_pos < 0) {
		behind_out_pos += emu_uart_fifo_LENGTH;
	}
	return (fifo->in_pos == behind_out_pos);
}

static int
_fifo_push(struct emu_uart_fifo * fifo, uint8_t data) {
	if (_fifo_full(fifo)) {
		return -1;
	}

	fifo->data[fifo->in_pos] = data;
	fifo->in_pos++;
	fifo->in_pos %= emu_uart_fifo_LENGTH;
	return 0;
}

static int
_fifo_pop(struct emu_uart_fifo * fifo, uint8_t * out_data) {
	if (_fifo_empty(fifo)) {
		return -1;
	}

	*out_data = fifo->data[fifo->out_pos];
	fifo->out_pos++;
	fifo->out_pos %= emu_uart_fifo_LENGTH;
	return 0;
}

static int
_divisor_latched(struct emu_uart * state) {
	return state->lcr & (1<<7);
}

static void
_print_binary(uint8_t value) {
	for (int i=7; i>=0; i--) {
		putchar('0' + ((value >> i) & 1));
	}
}

static float
_baud(struct emu_uart * state) {
	/*
	Datasheet formula: divisor = clock / (baud * 16) (see [1] table 3)
	Solved for baud: baud = clock / (divisor * 16)
	Example w/ 1.8432Mhz clock: 1843200 / (12 * 16) = 9600
	*/
	return ((float)state->clock_hz) / (state->dl * 16);
}

static int
_bitsPerChar(struct emu_uart * state) {
	/* (see [1] page 17) */
	int wordLength = 0;
	switch (state->lcr & 0b11) {
		case 0b00:
			wordLength = 5;
			break;
		case 0b01:
			wordLength = 6;
			break;
		case 0b10:
			wordLength = 7;
			break;
		case 0b11:
			wordLength = 8;
			break;
	}

	/* (see [1] page 17) */
	int stopBits = 0;
	if (!(state->lcr & (1<<2))) {
		stopBits = 1;
	}
	else {
		stopBits = 2;  /* technically could be 1.5 or 2 but I rounded */
	}

	/* (see [1] page 17) */
	int parityBits = 0;
	if (state->lcr & (1<<3)) {
		parityBits = 1;
	}

	return wordLength + stopBits + parityBits;
}

static void
_clocksPerChar_update(struct emu_uart * state) {
	float clocksPerBit = state->clock_hz / _baud(state);
	state->clocksPerChar = clocksPerBit * _bitsPerChar(state);
}

static void
_char_write(uint8_t ch) {
	putchar(ch);
}

void
emu_uart_dump(struct emu_uart * state) {
	// TODO FIFO print
	printf("IER: "); _print_binary(state->ier); printf("\n");
	printf("ISR: "); _print_binary(state->isr); printf("\n");
	printf("FCR: "); _print_binary(state->fcr); printf("\n");
	printf("LCR: "); _print_binary(state->lcr); printf("\n");
	printf("MCR: "); _print_binary(state->mcr); printf("\n");
	printf("LSR: "); _print_binary(state->lsr); printf("\n");
	printf("MSR: "); _print_binary(state->msr); printf("\n");
	printf("SPR: "); _print_binary(state->spr); printf("\n");
	printf("DLL: "); _print_binary(state->dl       ); printf("\n");
	printf("DLM: "); _print_binary((state->dl >> 8)); printf("\n");

	printf("clock: %i hz" "\n", state->clock_hz);
	printf("baud: %f" "\n", _baud(state));
	printf("clocks per char: %i" "\n", state->clocksPerChar);
	printf("TX clock counter: %i" "\n", state->tx_counter);
}

void
emu_uart_reset(struct emu_uart * state) {
	/* register reset states (see: [1] table 4) */
	struct emu_uart * s = state;
	s->ier = 0x00;
	s->fcr = 0x00;
	s->isr = 0x01;
	s->lcr = 0x00;
	s->mcr = 0x00;
	s->lsr = 0x60;
	s->msr &= 0xF0;
	s->spr = 0xFF;
}

void
emu_uart_init(struct emu_uart * state, void (*char_write)(uint8_t ch), int clock_hz) {
	state->char_write = char_write;
	state->clock_hz = clock_hz;

	// TODO is MSR upper nibble random at boot?
	// TODO is divisor latch random at boot?

	state->tx_counter = 0;

	// TODO is FIFO data random at boot?
	_fifo_init(&(state->rx_fifo));
	_fifo_init(&(state->tx_fifo));

	emu_uart_reset(state);
}

void
emu_uart_init_default(struct emu_uart * state) {
	emu_uart_init(state, &_char_write, /*clock_hz=*/1843200);
}

void
emu_uart_tick(struct emu_uart * state) {
	if (!_fifo_empty(&(state->tx_fifo))) {
		state->tx_counter++;

		if (state->tx_counter >= state->clocksPerChar) {
			/* NOTE does not enforce word size */
			uint8_t ch = '\0';
			_fifo_pop(&(state->tx_fifo), &ch);
			state->char_write(ch);

			state->tx_counter = 0;
		}
	}
}

uint8_t
emu_uart_read(struct emu_uart * state, uint8_t address) {
	if (!_divisor_latched(state)) {
		int status;
		uint8_t data = 0;
		switch (address) {
			case _RHR:
				status = _fifo_pop(&(state->rx_fifo), &data);
				if (status < 0) {
					// TODO handle RX FIFO underflow
				}
				return data;
			// TODO other registers
			case _LCR:
				return state->lcr;
			case _SPR:
				return state->spr;
			default:
				fprintf(stderr, "WARN: (UART) attempted read of invalid register 0x%02X" "\n", address);
				return 0xFF;
		}
	}
	else {
		switch (address) {
			case _DLL:
				return state->dl;
			case _DLM:
				return state->dl >> 8;
			// TODO which registers are not shadowed in this mode?
			case _LCR:
				return state->lcr;
			default:
				fprintf(stderr, "WARN: (UART) attempted read of invalid divisor register 0x%02X" "\n", address);
				return 0xFF;
		}
	}
}

void
emu_uart_write(struct emu_uart * state, uint8_t address, uint8_t data) {
	if (!_divisor_latched(state)) {
		int status;
		switch (address) {
			case _THR:
				status = _fifo_push(&(state->tx_fifo), data);
				if (status < 0) {
					// TODO handle TX FIFO overflow
				}
				return;
			// TODO other registers
			case _LCR:
				state->lcr = data;
				return;
			case _SPR:
				state->spr = data;
				return;
			default:
				fprintf(stderr, "WARN: (UART) attempted write to invalid register 0x%02X" "\n", address);
				return;
		}
	}
	else {
		switch (address) {
			case _DLL:
				state->dl = (state->dl & 0xFF00) | data;
				_clocksPerChar_update(state);
				return;
			case _DLM:
				state->dl = (state->dl & 0x00FF) | (data << 8);
				_clocksPerChar_update(state);
				return;
			// TODO which registers are not shadowed in this mode?
			case _LCR:
				state->lcr = data;
				return;
			default:
				fprintf(stderr, "WARN: (UART) attempted write to invalid divisor register 0x%02X" "\n", address);
				return;
		}
	}
}

void
emu_uart_receive(struct emu_uart * state, uint8_t data) {
	int status;
	status = _fifo_push(&(state->rx_fifo), data);
	if (status < 0) {
		// TODO handle RX FIFO overflow
	}
}
