#include "lib/z80/z80.h"

#include <inttypes.h>

#include <SDL.h>

#include <SDL_framerate.h>

#include <stdlib.h>

#include <stdio.h>

#include "emu.h"

#define _sdl_error(SDL_FUNCTION_NAME) \
	fprintf(stderr, "ERROR: %s() at %s:%s():%i -- %s" "\n", (SDL_FUNCTION_NAME), __FILE__, __func__, __LINE__, SDL_GetError());

static struct {
	SDL_Surface * screen;
	FPSmanager fpsManager;
} _sdl;

static void
_file_readAll(const char * path, uint8_t * * out_data, size_t * out_data_size) {
	FILE * file = fopen(path, "r");
	if (!file) {
		perror(path);
		exit(1);
	}

	fseek(file, 0, SEEK_END);
	size_t data_size = ftell(file);
	fseek(file, 0, SEEK_SET);

	uint8_t * data = malloc(data_size);
	if (!data) {
		perror(NULL);
		exit(1);
	}

	size_t fread_amount = fread(data, 1, data_size, file);
	if (fread_amount != data_size) {
		fprintf(stderr, "ERROR: tried to read %zi bytes from \"%s\" but only got %zi bytes" "\n", data_size, path, fread_amount);
		exit(1);
	}

	int status = fclose(file);
	if (status != 0) {
		perror(path);
		exit(1);
	}

	*out_data      = data;
	*out_data_size = data_size;
}


static void
_init(const char * bios_PATH) {
	int status;

	status = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK);
	if (status < 0) {
		_sdl_error("SDL_Init");
		exit(1);
	}

	_sdl.screen = SDL_SetVideoMode(256, 240, /*bpp=*/8, /*flags=*/SDL_DOUBLEBUF);
	if (!_sdl.screen) {
		_sdl_error("SDL_SetVideoMode");
		exit(1);
	}

	SDL_initFramerate(&_sdl.fpsManager);

	status = SDL_setFramerate(&_sdl.fpsManager, /*framerate=*/60);
	if (status < 0) {
		_sdl_error("SDL_setFramerate");
		exit(1);
	}

	uint8_t * bios;
	size_t    bios_size;
	_file_readAll(bios_PATH, &bios, &bios_size);
	emu_init(bios, bios_size);
	free(bios);
}

static void
_halt(void) {
	SDL_Quit();
}
int
main(int argc, char * argv[]) {
	if (argc < 2) {
		fprintf(stderr, "Syntax: %s <bios>" "\n", argv[0]);
		return 1;
	}
	const char * argv_BIOS = argv[1];

	_init(argv_BIOS);

	emu_dump_mem(0x0000, 512);
	emu_dump_cpu();
	printf("====RUN================================" "\n");
	for (int i=0; i<50000; i++) {
		emu_step();
	}
	printf("\n" "====DONE===============================" "\n");
	emu_dump();

	_halt();

	return 0;
}
