#include "lib/z80/z80.h"

#include <inttypes.h>

#include <stdlib.h>

#include <stdio.h>

#include "emu_uart.h"

#include "emu.h"

#define _rom_SIZE ( 16*1024)
#define _ram_SIZE (512*1024)

#define _mem_rom_START     0x0000
#define _mem_ram_START     0x4000
#define _mem_bankram_START 0x8000

// TODO other io devcies
#define _io_device_BANKSWITCH 0b000
#define _io_device_UART       0b001

static struct {
	z80 cpu;
	uint8_t rom[_rom_SIZE];
	uint8_t ram[_ram_SIZE];

	uint8_t bank;

	struct emu_uart uart;
} _emu;

static uint8_t *
_mem_decode(uint16_t addr) {
	uint16_t addr_upper = addr >> 14;
	uint16_t addr_lower14 = addr & 0b0011111111111111;
	uint16_t addr_lower15 = addr & 0b0111111111111111;
	switch (addr_upper) {
		case 0b00:
			return _emu.rom + addr_lower14;
		case 0b01:
			return _emu.ram + addr_lower14;
		case 0b10:
		case 0b11:
			return _emu.ram + (_emu.bank << 15) + addr_lower15;
	}

	return _emu.rom;
}

static uint8_t
_z80_mem_read(void * userdata, uint16_t addr) {
	(void)userdata;

	uint8_t * mem = _mem_decode(addr);
	return *mem;
}

static void
_z80_mem_write(void * userdata, uint16_t addr, uint8_t data) {
	(void)userdata;

	if (addr >= _mem_ram_START) {
		uint8_t * mem = _mem_decode(addr);
		*mem = data;
	}
}

void
_z80_io_port_decode(uint8_t port, uint8_t * out_device, uint8_t * out_addr) {
	*out_device = port >> 5;
	*out_addr   = port & 0b11111;
}

static uint8_t
_z80_io_read(z80 * cpu, uint8_t port) {
	(void)cpu;

	uint8_t device;
	uint8_t addr;
	_z80_io_port_decode(port, &device, &addr);
	switch (device) {
		case _io_device_UART:
			return emu_uart_read(&_emu.uart, addr);
			break;
		default:
			fprintf(stderr, "WARN: IO device %i has no read implemented" "\n", device);
			break;
	}

	return 0xAA;
}

static void
_z80_io_write(z80 * cpu, uint8_t port, uint8_t data) {
	(void)cpu;

	uint8_t device;
	uint8_t addr;
	_z80_io_port_decode(port, &device, &addr);
	switch (device) {
		case _io_device_BANKSWITCH:
			_emu.bank = data;
			break;
		case _io_device_UART:
			emu_uart_write(&_emu.uart, addr, data);
			break;
		default:
			fprintf(stderr, "WARN: IO device %i has no write implemented" "\n", device);
			break;
	}
}

static void
_cpu_init(void) {
	z80_init(&_emu.cpu);
	_emu.cpu.read_byte  = _z80_mem_read;
	_emu.cpu.write_byte = _z80_mem_write;
	_emu.cpu.port_in    = _z80_io_read;
	_emu.cpu.port_out   = _z80_io_write;
}

void
emu_init(const uint8_t * bios, size_t bios_size) {
	size_t bios_end = bios_size;
	if (bios_end > _rom_SIZE) {
		fprintf(stderr, "WARN: BIOS image is too large: %zi" "\n", bios_size);
		bios_end = _rom_SIZE;
	}
	for (uint32_t i=0; i<bios_end; i++) {
		_emu.rom[i] = bios[i];
	}
	// fill remaining ROM space with default value for erased flash ROM
	for (uint32_t i=bios_end; i<_rom_SIZE; i++) {
		_emu.rom[i] = 0xFF;
	}

	for (uint32_t i=0; i<_ram_SIZE; i++) {
		_emu.ram[i] = 0x00;  // TODO? randomize
	}

	_cpu_init();

	_emu.bank = 0x00;

	emu_uart_init_default(&_emu.uart);

	emu_reset();

	// TODO remove this debug code
	/*
	_z80_io_write(NULL, 0b00100011, 0x80);  // LCR
	_z80_io_write(NULL, 0b00100000, 0x0C);  // DLL
	_z80_io_write(NULL, 0b00100001, 0x00);  // DLM
	_z80_io_write(NULL, 0b00100011, 0x00);  // LCR
	_z80_io_write(NULL, 0b00100000, '?');   // THR
	for (int i=0; i<1152; i++) {
		emu_uart_tick(&_emu.uart);
	}
	*/
}

void
emu_dump_cpu(void) {
	z80_debug_output(&_emu.cpu);
}

void
emu_dump_mem(uint16_t start, uint16_t amount) {
	const int WIDTH = 16;

	uint32_t bank_low  = (uint32_t)(_mem_decode(0x8000) - _emu.ram);
	uint32_t bank_high = (uint32_t)(_mem_decode(0xFFFF) - _emu.ram);
	printf("(bank=%02X (%06X-%06X))" "\n", _emu.bank, bank_low, bank_high);

	for (uint16_t i=0; i<amount; i++) {
		uint16_t addr = start + i;
		uint8_t  data = _z80_mem_read(/*userdata=*/NULL, addr);
		
		int x = i % WIDTH;
		if (x == 0) {
			printf("%04X  ", addr);
		}
		printf("%02X", data);
		if (x == (WIDTH - 1)) {
			printf("\n");
		}
		else {
			printf(" ");
		}
	}
	printf("\n");
}

void
emu_dump(void) {
	emu_dump_cpu();

	emu_uart_dump(&_emu.uart);
}

void
emu_reset(void) {
	_cpu_init();  // TODO? manually implement reset

	emu_uart_reset(&_emu.uart);
}

void
emu_step(void) {
	z80_step(&_emu.cpu);

	emu_uart_tick(&_emu.uart);
}
