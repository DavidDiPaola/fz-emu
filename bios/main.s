/* 2022 David DiPaola, licensed CC0 (public domain worldwide) */

/* event handlers (see [1] page 6) */
.org 0x0000  /* reset handler */
	jp init
.org 0x0066  /* NMI handler */
	jp init  /* TODO actually handle this */

.org 0x0100  /* TODO figure out where code should go */

init:
	/* set SP to the top of un-banked RAM */
	ld sp, 0x7FFE

	/* set RAM bank to 0x008000-0x00FFFF */
	ld a, 0x01
	out (0b00000000), a

	/* set serial baud rate to 9600 */
	ld de, 0x000C
	call uart_baud_set

	/*
	ld d, '?'
	call uart_write_char
	*/
	ld hl, message
	call uart_write_str

	jp .

message:
	.string "Hello world!"

uart_baud_set:
	push af

	/* turn on divisor latch access */
	ld a, 0x80
	out (0b00100011), a

	/* set divisor LSB */
	ld a, e
	out (0b00100000), a

	/* set divisor MSB */
	ld a, d
	out (0b00100001), a

	/* turn off divisor latch access */
	ld a, 0x00
	out (0b00100011), a

	pop af
	ret

/*
Write a character via the serial port.
	d - the character to be sent.
*/
uart_write_char:
	push af

	/* TODO check if TX FIFO is full */

	/* push char to UART TX FIFO */
	ld a, d
	out (0b00100000), a
	
	pop af
	ret

/*
Write a string via the serial port.
	hl - the address of the string to be sent.
*/
uart_write_str:
	push af
	push hl

1:
	/* TODO check if TX FIFO is full */
	ld a, (hl)           /* get character from string */
	cp 0                 /* is it NUL? */
	jp z, 2f             /* if so, quit */
	out (0b00100000), a  /* push char to UART TX FIFO */
	inc hl               /* point to next character */
	jp 1b

2:
	pop hl
	pop af
	ret

/*
References
	[1] Z80 CPU User Manual UM008011-0816 Revision 11
*/
