SRC = src/main.c src/emu.c src/emu_uart.c

LIB_Z80 = src/lib/z80
LIB_Z80_OBJ = $(LIB_Z80)/z80.o

BIN = fz-emu

OBJ = $(SRC:.c=.o)

_CFLAGS = \
	-std=c99 -fwrapv -Wall -Wextra \
	$(shell sdl-config --cflags) \
	-g \
	$(CFLAGS)
_LDFLAGS = \
	-O1 \
	$(LDFLAGS)
_LDFLAGS_LIB = \
	-lm \
	$(shell sdl-config --libs) -lSDL_gfx \
	$(LDFLAGS_LIB)

.PHONY: all
all: $(BIN)

.PHONY: clean
clean:
	$(MAKE) -C $(LIB_Z80) clean
	rm -f $(OBJ)
	rm -f $(BIN)

.PHONY: lib_z80
lib_z80:
	$(MAKE) -C $(LIB_Z80)

.PHONY: bios
bios:
	$(MAKE) -C bios

%.o: %.c
	$(CC) $(_CFLAGS) -c $< -o $@

$(LIB_Z80_OBJ): lib_z80

$(LIB_Z80_H): lib_z80

$(BIN): $(OBJ) $(LIB_Z80_OBJ)
	$(CC) $(_LDFLAGS) $^ -o $@ $(_LDFLAGS_LIB)
